﻿using System.IO;
using System.Text;
using TMPro;
using UnityEngine;

public class InputScript : MonoBehaviour
{
    private int intVariable;
    private float floatVariable;
    private char charVariable;
    private bool boolVariable;

    private string result;

    public TMP_InputField IntField;
    public TMP_InputField FloatField;
    public TMP_InputField CharField;
    public TMP_InputField BoolField;

    public void OnSave()
    {
        int.TryParse(IntField.text, out intVariable);
        float.TryParse(FloatField.text, out floatVariable);
        char.TryParse(CharField.text, out charVariable);
        bool.TryParse(BoolField.text, out boolVariable);

        StringBuilder sb = new StringBuilder();
        sb.AppendLine($"Начальные значения:");
        sb.AppendLine($"int: {intVariable}");
        sb.AppendLine($"float: {floatVariable}");
        sb.AppendLine($"char: {charVariable}");
        sb.AppendLine($"bool: {boolVariable}");

        sb.AppendLine();

        sb.AppendLine($"Преобразования:");
        sb.AppendLine($"int to float: {(float)intVariable}");
        sb.AppendLine($"float to int: {(int)floatVariable}");
        sb.AppendLine($"char to int: {(int)charVariable}");
        sb.AppendLine($"char to float: {(float)charVariable}");

        result = sb.ToString();

        var currentDir = Directory.GetCurrentDirectory();
        File.WriteAllText(Path.Combine(currentDir, "File.txt"), result);
    }
}
